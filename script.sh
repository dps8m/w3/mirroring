#!/usr/bin/env sh
# SPDX-License-Identifier: MIT-0
# Copyright (c) 2024 The DPS8M Development Team

# This script expects GNU (not BSD/macOS) tooling!

# verbose
set -x

# get mirror copy
rm -f multics.tar.gz
lftpget -v ftp://multicians.org/multics.tar.gz
cp -f multics.tar.gz multicians.org-$(date +%s).tar.gz

# extract and cleanup
cd multicians.org
env rm -rf public/*
cd public
tar zxvf ../../multics.tar.gz
rm -f ../../multics.tar.gz
test -f jquery.fancybox.css || curl -fsSLO https://multicians.org/jquery.fancybox.css

# ensure no files >99MB (gitlab limitation)
find . -type f -size +99M

# move MTBs
rm -rf ../../mtbs/public
mv mtbs ../../mtbs/public

# move images
rm -rf ../../mulimg/public
mv mulimg ../../mulimg/public

# rewrite mtbs/ paths
grep -rl 'href="mtbs/' . | xargs sed -i 's#href="mtbs/#href="/w3/mtbs/#g'
grep -rl 'href="mulimg/' . | xargs sed -i 's#href="mulimg/#href="/w3/mulimg/#g'
grep -rl 'img src="mulimg/' . | xargs sed -i 's#img src="mulimg/#img src="/w3/mulimg/#g'
grep -rl 'img src="\.\./\.\./mulimg/' . | xargs sed -i 's#img src="\.\./\.\./mulimg/#img src="/w3/mulimg/#g'
grep -rl 'img src="\.\./mulimg/' . | xargs sed -i 's#img src="\.\./mulimg/#img src="/w3/mulimg/#g'
grep -rl '(mulimg/' . | xargs sed -i 's#(mulimg/#(/w3/mulimg/#g'
grep -rl 'srcset="mulimg/' . | xargs sed -i 's#srcset="mulimg/#srcset="/w3/mulimg/#'
grep -rl ', mulimg/' . | xargs sed -i 's#, mulimg/#, /w3/mulimg/#g'
grep -rl "'mulimg/" . | xargs sed -i "s#'mulimg/#'/w3/mulimg/#g"
grep -rl '="mulimg/' . | xargs sed -i 's#="mulimg/#="/w3/mulimg/#g'

# add cloudflare tracking
grep -rl '</body>' . | xargs sed -i "s#</body>#<script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{\"token\": \"4b6a131644014aa29fd88953508c3a3d\"}'></script></body>#"

cd ..

# create compressed files for gitlab pages
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k -v -Z -- "{}" \;
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec pigz -9 -f -k -m -v -- "{}" \;

# create "multicians.org" repo
cp ../ci-config.txt .gitlab-ci.yml
cp ../MIT.txt LICENSE
git init
git add -A
git commit -asS -m "Initial import"
git remote add origin git@gitlab.com:dps8m/w3/multicians.org.git
git push --set-upstream origin master -f

# create "mtbs" repo
cp ../ci-config.txt .gitlab-ci.yml
cp ../MIT.txt LICENSE
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k -v -Z -- "{}" \;
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec pigz -9 -f -k -m -v -- "{}" \;
git init
git add -A
git commit -asS -m "Initial import"
git remote add origin git@gitlab.com:dps8m/w3/mtbs.git
git push --set-upstream origin master -f

# create "mulimg" repo
cp ../ci-config.txt .gitlab-ci.yml
cp ../MIT.txt LICENSE
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k -v -Z -- "{}" \;
find "public" -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec pigz -9 -f -k -m -v -- "{}" \;
git init
git add -A
git commit -asS -m "Initial import"
git remote add origin git@gitlab.com:dps8m/w3/mulimg.git
git push --set-upstream origin master -f
